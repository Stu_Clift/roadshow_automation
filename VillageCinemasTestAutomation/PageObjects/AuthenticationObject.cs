﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace VillageCinemasTestAutomation.PageObjects
{
    public class AuthenticationObject
    {
        private readonly IWebDriver driver;
        public AuthenticationObject(IWebDriver browser)

        {
            this.driver = browser;
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.Id, Using = "username_placeholder")]
        public IWebElement FirstUsernamePlaceholder { get; set; }

        [FindsBy(How = How.Name, Using = "lsubmit")]
        public IWebElement FirstSubmitButton { get; set; }

        [FindsBy(How = How.Name, Using = "passsfc")]
        public IWebElement PasswordField { get; set; }

        [FindsBy(How = How.Name, Using = "bsubmit")]
        public IWebElement SecondSubmitButton { get; set; }

        [FindsBy(How = How.Name, Using = "button")]
        public IWebElement AcceptButton { get; set; }

        public void HandleZscaler()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(driver => !driver.FindElement(By.Id("username_placeholder")).Displayed);
            System.Threading.Thread.Sleep(10000);
            Actions actions = new Actions(driver);
            actions.MoveToElement(FirstUsernamePlaceholder);
            actions.Click();
            //Your email
            actions.SendKeys("");
            actions.Build().Perform();
            FirstSubmitButton.Click();
            wait.Until(driver => !driver.FindElement(By.Name("lognsfc")).Displayed);
            actions.MoveToElement(PasswordField);
            actions.Click();
            //Your Pass
            actions.SendKeys("");
            actions.Build().Perform();
            SecondSubmitButton.Click();
            AcceptButton.Click();
        }
    }
}