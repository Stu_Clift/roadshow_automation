﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace VillageCinemasTestAutomation.PageObjects
{
    public class DesktopLoginPage
    {
        private readonly IWebDriver driver;
        public DesktopLoginPage(IWebDriver browser)

        {
            this.driver = browser;
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.Id, Using = "content_0_leftcontent_0_tb_Email")]
        public IWebElement EmailAddressTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "content_0_leftcontent_0_tb_Password")]
        public IWebElement YourPasswordTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "uniform-content_0_leftcontent_0_btnLogIn")]
        public IWebElement LoginButton { get; set; }

        public void EnterLogInEmail(string textToType)
        {
            new Actions(driver).MoveToElement(EmailAddressTextBox);
            EmailAddressTextBox.Clear();
            EmailAddressTextBox.SendKeys(textToType);
        }

        public void EnterLogInPassword(string textToType)
        {
            new Actions(driver).MoveToElement(YourPasswordTextBox);
            YourPasswordTextBox.Clear();
            YourPasswordTextBox.SendKeys(textToType);
        }

        public void ClickLogInButton()
        {
            new Actions(driver).MoveToElement(LoginButton);
            LoginButton.Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(driver => !driver.FindElement(By.Id("ucLoader_imgLoading")).Displayed);
            System.Threading.Thread.Sleep(5000);
        }


    }
}