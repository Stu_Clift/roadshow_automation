﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace VillageCinemasTestAutomation.PageObjects
{

    public class VillageCinemasMobileObjects
    {

        private readonly IWebDriver driver;
        private readonly string url = @"http://awscdn.scuat.villagecinemas.com.au/?lg=Mobile";
        public VillageCinemasMobileObjects(IWebDriver browser)


        {
            this.driver = browser;
            PageFactory.InitElements(browser, this);
        }

        public void NavigateToMobileHomePage()
        {
            this.driver.Navigate().GoToUrl(this.url);
        }

        //*******************//
        //MOBILE LANDING PAGE//
        //*******************//

        public void SelectMobileLoginButton()
        {
            var logInIcon = driver.FindElement(By.CssSelector("div.icon-set.profile"));
            logInIcon.Click();
        }

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement MobileLogInEmailField { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement MobileLogInPasswordField { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@value='Submit']")]
        public IWebElement MobileLogInButton { get; set; }


        [FindsBy(How = How.XPath, Using = "//a[@href='/village-movie-club/my-vmc']")]
        public IWebElement MobileMyVmcButton { get; set; }


        public void SetEmail(string textToType)
        {
            MobileLogInEmailField.Clear();
            MobileLogInEmailField.SendKeys(textToType);
            MobileLogInEmailField.Click();
        }

        public void SetPassword(string textToType)
        {
            MobileLogInPasswordField.Clear();
            MobileLogInPasswordField.SendKeys(textToType);
            MobileLogInPasswordField.Click();
        }

        public void ClickMobileLoginButton()
        {
            MobileLogInButton.Click();
            System.Threading.Thread.Sleep(8000);
        }

        public void SelectMobileMyVmcButton()
        {
            var myVmcLogInIcon = driver.FindElement(By.XPath("//a[@class='vmc ng-scope']"));
            myVmcLogInIcon.Click();
        }

        //This method selects a ticket from the mobile landing page
        public void SelectMobileTicket()
        {
            driver.FindElement(By.CssSelector("span.ng-scope")).Click();
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.XPath("//div[@id='modal-popup']/div/div[3]/div/div/div/div/div")).Click();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.PageDown).Perform();
            System.Threading.Thread.Sleep(1000);
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.PageDown).Perform();
            System.Threading.Thread.Sleep(1000);
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.PageDown).Perform();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.PageDown).Perform();
            driver.FindElement(By.XPath("//div[@id='modal-popup']/div/div[3]/div/div/div[2]/ul[22]/ng-include/li/a/div")).Click();
            System.Threading.Thread.Sleep(3000);
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.PageDown).Perform();
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.CssSelector("img[alt=\"3D Fantastic Beasts and Where to Find Them\"]")).Click();
            System.Threading.Thread.Sleep(3000);
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.ArrowDown).Perform();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.ArrowDown).Perform();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.ArrowDown).Perform();
            driver.FindElement(By.XPath("//a[contains(.,'12:10 AM')]")).Click();
            System.Threading.Thread.Sleep(8000);
        }

        public void SelectQtyMobileTickets()
        {
            driver.FindElement(By.XPath("//div[@id='TicketingCtrl']/ng-view/ng-include[2]/div/div[2]/ul/ng-include[3]/li/div[3]/a/div")).Click();
            Actions actions = new Actions(driver);
            IWebElement nextButton = driver.FindElement(By.XPath("//*[@id='TicketingCtrl']/ng-view/div[6]/div[2]/div/a"));           
            new Actions(driver).MoveToElement(nextButton);
            actions.Perform();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.XPath("//*[@id='TicketingCtrl']/ng-view/div[6]/div[2]/div/a")).Click();
            System.Threading.Thread.Sleep(12000);
        }


        public void MakeCreditCardMobilePayment()
        {
            IWebElement accordianCreditCard = driver.FindElement(By.Id("accordion-CreditCard"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(accordianCreditCard);
            actions.Perform();
            driver.FindElement(By.Id("accordion-CreditCard")).Click();
            System.Threading.Thread.Sleep(5000);
            IWebElement creditCardName = driver.FindElement(By.Id("cc_cardName"));
            actions.MoveToElement(creditCardName);
            actions.Perform();
            driver.FindElement(By.Id("cc_email")).Clear();
            driver.FindElement(By.Id("cc_email")).SendKeys("email@email.com");
            driver.FindElement(By.Id("cc_phoneNumber")).Clear();
            driver.FindElement(By.Id("cc_phoneNumber")).SendKeys("0444444444");
            driver.FindElement(By.Id("cc_cardName")).Clear();
            driver.FindElement(By.Id("cc_cardName")).SendKeys("fred name");
            driver.FindElement(By.Id("cc_cardNumber")).Clear();
            driver.FindElement(By.Id("cc_cardNumber")).SendKeys("4111111111111111");
            driver.FindElement(By.Id("cc_cvc")).Clear();
            driver.FindElement(By.Id("cc_cvc")).SendKeys("1234");
            new SelectElement(driver.FindElement(By.Id("cc_monthExpiry"))).SelectByText("05");
            new SelectElement(driver.FindElement(By.Id("cc_yearExpiry"))).SelectByText("2017");
            IWebElement buyTicketsButton = driver.FindElement(By.Id("cc_tnc_label"));
            actions.MoveToElement(buyTicketsButton);
            actions.Perform();
            IWebElement checkBox = driver.FindElement(By.Id("cc_tnc_cbx"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click()", checkBox);
            System.Threading.Thread.Sleep(3000);
            var buyTickets = driver.FindElement(By.XPath("//*[@id='CreditcardPaymentCtrl']/form/div[3]/button/span"));
            buyTickets.Click();
            System.Threading.Thread.Sleep(20000);
        }

        public void MakeMasterPassMobilePayment()
        {
            IWebElement accordianMasterPass = driver.FindElement(By.Id("accordion-Masterpass"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(accordianMasterPass);
            actions.Perform();
            driver.FindElement(By.Id("accordion-Masterpass")).Click();
            System.Threading.Thread.Sleep(3000);
            //Using Javascript to execute the checkbox click as the element is hidden
            IWebElement checkBox = driver.FindElement(By.Id("mp_tnc_cbx"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click()", checkBox);
            string oldWindow = driver.WindowHandles[0];
            var buyWithMasterPassButton = driver.FindElement(By.XPath("//*[@id='MasterpassPaymentCtrl']/div[2]/div[2]/div/div[2]/a/img"));
            actions.MoveToElement(buyWithMasterPassButton);
            buyWithMasterPassButton.Click();
            System.Threading.Thread.Sleep(14000);
            driver.SwitchTo().Frame(driver.FindElement(By.Id("MasterPass_frame")));
            System.Threading.Thread.Sleep(10000);
            IWebElement masterPassLabelButton = driver.FindElement(By.XPath("//div[@id='featured-wallets-logos']/div[3]/div[3]/button"));
            masterPassLabelButton.Click();
            System.Threading.Thread.Sleep(10000);
            driver.SwitchTo().Frame(driver.FindElement(By.Id("MasterPass_wallet_frame")));
            driver.FindElement(By.Id("email")).SendKeys("john.berry@accesshq.com");
            driver.FindElement(By.Id("password")).SendKeys("Password1");
            driver.FindElement(By.Id("signInButton")).Click();
            System.Threading.Thread.Sleep(20000);
            driver.FindElement(By.CssSelector("button.button.command")).Click();
            System.Threading.Thread.Sleep(30000);
            driver.SwitchTo().Window(oldWindow);
        }

        public void MakePayPalMobilePayment()
        {
            //Select PayPal payment option
            IWebElement accordianPayPal = driver.FindElement(By.Id("accordion-PayPal"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(accordianPayPal);
            actions.Perform();
            driver.FindElement(By.Id("accordion-PayPal")).Click();
            System.Threading.Thread.Sleep(3000);

            //Using Javascript to execute the checkbox click as the element is hidden
            IWebElement checkBox = driver.FindElement(By.Id("pp_tnc_cbx"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click()", checkBox);

            //Paypal starts here
            var payPalButton = driver.FindElement(By.XPath("//*[@id='imgPaypalBtn']"));
            actions.MoveToElement(payPalButton);
            payPalButton.Click();
            System.Threading.Thread.Sleep(14000);
            driver.Manage().Window.Maximize();

            //The PayPal screen is present at this point  
            System.Threading.Thread.Sleep(8000);
            driver.SwitchTo().Frame("injectedUl");
            driver.FindElement(By.Id("email")).Click();
            driver.FindElement(By.XPath("//*[@id='email']")).SendKeys("testing1@profero.com");
            driver.FindElement(By.Id("password")).Click();
            driver.FindElement(By.XPath("//*[@id='password']")).SendKeys("Password1");
            driver.FindElement(By.Id("btnLogin")).Click();
            System.Threading.Thread.Sleep(15000);
            driver.FindElement(By.XPath("//*[@id='closeCart']")).Click();
            driver.FindElement(By.XPath("//*[@id='confirmButtonTop']")).Click();
            System.Threading.Thread.Sleep(20000);
            driver.FindElement(By.XPath("//p[contains(.,'Show this code on your phone or print it out to access the cinema.  Booking details have also been emailed to you.')]"));
        }

        public void MakeGiftCardMobilePayment()
        {
            IWebElement accordianGiftCard = driver.FindElement(By.Id("accordion-GiftCard"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(accordianGiftCard);
            actions.Perform();
            accordianGiftCard.Click();
            //System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.Id("gc_cardName")).Clear();
            driver.FindElement(By.Id("gc_cardName")).SendKeys("Mr Card Name");
            driver.FindElement(By.Id("gc_email")).Clear();
            driver.FindElement(By.Id("gc_email")).SendKeys("email@email.com");
            driver.FindElement(By.Id("gc_phoneNumber")).Clear();
            driver.FindElement(By.Id("gc_phoneNumber")).SendKeys("0444444444");
            driver.FindElement(By.Id("gc_cardNumber")).Clear();
            driver.FindElement(By.Id("gc_cardNumber")).SendKeys("9036007706232620");
            driver.FindElement(By.Id("gc_cvc")).Clear();
            driver.FindElement(By.Id("gc_cvc")).SendKeys("1234");
            IWebElement checkBox = driver.FindElement(By.Id("gc_tnc_cbx"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click()", checkBox);
            //System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@id='GiftcardPaymentCtrl']/form/div[3]/button/span")).Click();
            System.Threading.Thread.Sleep(20000);
        }

        public void MakeVisaCheckoutMobilePayment()
        {
            //Select Visa Checkout payment option
            IWebElement accordianVisaCheckout = driver.FindElement(By.Id("accordion-VisaCheckout"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(accordianVisaCheckout);
            actions.Perform();
            driver.FindElement(By.Id("accordion-VisaCheckout")).Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(driver => driver.FindElement(By.Id("vp_tnc_cbx")).Displayed);
            IWebElement checkBox = driver.FindElement(By.Id("vp_tnc_cbx"));
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click()", checkBox);
            string oldWindow = driver.WindowHandles[0];
            var visaCheckoutButton = driver.FindElement(By.XPath("//*[@id='imgVisaButton']"));
            actions.MoveToElement(visaCheckoutButton);
            visaCheckoutButton.Click();            
            driver.SwitchTo().Frame(driver.FindElement(By.Id("VMECheckoutIframe")));
            driver.FindElement(By.CssSelector("a > span")).Click();
            IWebElement username = driver.FindElement(By.Id("userName"));
            username.SendKeys("john_berry@vrl.com.au");
            IWebElement password = driver.FindElement(By.Id("password"));
            password.SendKeys("Password1");
            IWebElement signIn = driver.FindElement(By.ClassName("viewButton-button"));
            signIn.Click();
            IWebElement payButton = driver.FindElement(By.Name("Pay"));
            payButton.Click();           
            System.Threading.Thread.Sleep(30000);
        }
    }
}