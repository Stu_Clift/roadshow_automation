﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace VillageCinemasTestAutomation.PageObjects
{
    public class DesktopContactUsPage
    {
        private readonly IWebDriver driver;
        public DesktopContactUsPage(IWebDriver browser)

        {
            this.driver = browser;
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.Id, Using = "btnSubmit")]
        public IWebElement SubmitEnquiryButton { get; set; }

        public void ClickSubmitEnquiryButton()
        {
            new Actions(driver).MoveToElement(SubmitEnquiryButton);
            SubmitEnquiryButton.Click();
        }
    }
}