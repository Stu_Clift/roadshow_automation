﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace VillageCinemasTestAutomation.PageObjects
{

    public static class WebDriverExtensions
    {
        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return driver.FindElement(by);
        }
    }

    public class VillageCinemasDesktopObjects
    {
        private readonly IWebDriver driver;
        private readonly string url = @"http://awscdn.scuat.villagecinemas.com.au/";
        public VillageCinemasDesktopObjects(IWebDriver browser)


        {
            this.driver = browser;
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.Id, Using = "aHeaderQuickTicket")]
        public IWebElement HeaderQuickTicket { get; set; }

        [FindsBy(How = How.Id, Using = "ddlByCinemaCinemas")]
        public IWebElement HeaderQuickCinema { get; set; }

        [FindsBy(How = How.CssSelector, Using = "option[value=\"001\"]")]
        public IWebElement SelectStaging { get; set; }

        [FindsBy(How = How.Id, Using = "ddlByCinemaMovies")]
        public IWebElement HeaderQuickMovie { get; set; }

        [FindsBy(How = How.Name, Using = "Arrival")]
        public IWebElement SelectMovie { get; set; }

        [FindsBy(How = How.Id, Using = "ddlByCinemaSessions")]
        public IWebElement HeaderQuickSession { get; set; }

        [FindsBy(How = How.Id, Using = "qtCinemaBuyTickets")]
        public IWebElement QuickTicketsBuyTicketsButton { get; set; }

        [FindsBy(How = How.Id, Using = "txtGlobalSiteSearch")]
        public IWebElement SearchBox { get; set; }

        [FindsBy(How = How.Id, Using = "vrHdrSection_lbLogIn")]
        public IWebElement LogInLink { get; set; }

        [FindsBy(How = How.LinkText, Using = "Village Movie Club")]
        public IWebElement VillageMovieClubLink { get; set; }

        [FindsBy(How = How.LinkText, Using = "Edit Your Profile")]
        public IWebElement EditYourProfileLink { get; set; }

        [FindsBy(How = How.Id, Using = "txtFirstName")]
        public IWebElement YourDetailsFirstNameTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "txtCurrentPassword")]
        public IWebElement YourDetailsCurrentPasswordTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "chkPolicy")]
        public IWebElement YourDetailsPrivacyPolicyCheckbox { get; set; }

        [FindsBy(How = How.Id, Using = "chkTerms")]
        public IWebElement YourDetailsTermsAndConditionsCheckbox { get; set; }

        [FindsBy(How = How.Id, Using = "btnSave")]
        public IWebElement EditYourProfileSaveButton { get; set; }

        [FindsBy(How = How.LinkText, Using = "Contact Us")]
        public IWebElement ContactUsLink { get; set; }

        [FindsBy(How = How.LinkText, Using = "Vrewards")]
        public IWebElement VrewardsLink { get; set; }

        public void NavigateToDesktopHomePage()
        {
            this.driver.Navigate().GoToUrl(this.url);
        }

       
        //********************//
        //DESKTOP LANDING PAGE//
        //********************//

        public void ClickLoginLink()
        {
            new Actions(driver).MoveToElement(LogInLink);
            LogInLink.Click();
        }

        public void ClickQuickTicketsHeader()
        {
            new Actions(driver).MoveToElement(HeaderQuickTicket);
            HeaderQuickTicket.Click();
        }

        public void ClickQuickTicketsCinema()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(driver => driver.FindElement(By.Id("qtCinemaBuyTickets")).Displayed);
            var cinema = driver.FindElement(By.Name("ctl04$ddlByCinemaCinemas"));
            var selectQuickTicketCinema = new SelectElement(cinema);
            selectQuickTicketCinema.SelectByValue("001");
            System.Threading.Thread.Sleep(1000);
        }

        public void ClickQuickTicketsMovie()
        {
            var movie = driver.FindElement(By.Name("ctl04$ddlByCinemaMovies"));
            var selectQuickTicketMovie = new SelectElement(movie);
            selectQuickTicketMovie.SelectByValue("HO00009872");
            System.Threading.Thread.Sleep(1000);
        }

        public void ClickQuickTicketsSession()
        {
            driver.FindElement(By.Id("ddlByCinemaSessions")).Click();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.ArrowDown).Perform();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.Return).Perform();
        }

        public void ClickQuickTicketsBuyTicketsButton()
        {
            new Actions(driver).MoveToElement(QuickTicketsBuyTicketsButton);
            QuickTicketsBuyTicketsButton.Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(driver => driver.FindElement(By.XPath("//h1[contains(.,'Select Tickets')]")).Displayed);
        }

        //Capture Master Card Payment Option
        public void SelectMasterCardPayment()
        {
            driver.FindElement(By.XPath("//div[@id='pnlPaymentOptions']/ul/li[4]/div/img")).Click();
            driver.FindElement(By.Id("content_0_chkTerms")).Click();
            driver.FindElement(By.CssSelector("img[alt=\"Connect with MasterPass\"]")).Click();
            System.Threading.Thread.Sleep(10000);
        }

        public void SelectVisaCardPayment()
        {
            driver.FindElement(By.XPath("//div[@id='pnlPaymentOptions']/ul/li[3]")).Click();
            driver.FindElement(By.Id("content_0_chkTerms")).Click();
            driver.FindElement(By.CssSelector("img.v-button")).Click();
        }

        //*******************************//
        //MASTER CARD POP-UP PAYMENT PAGE//
        //*******************************//

        //Complete Master Card Pop Up Payment option
        public void CompleteMasterCardPopUpPayment()
        {
            //Wait for the "Loading" to not be displayed before proceeding
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(driver => !driver.FindElement(By.Id("MasterPass_animation_overlay")).Displayed);

            IWebElement masterPassLabelButton = driver.FindElement(By.XPath("//div[@id='featured-wallets-logos']/div[3]/div[3]/button"));
            masterPassLabelButton.Click();
            System.Threading.Thread.Sleep(10000);
            driver.SwitchTo().Frame(driver.FindElement(By.Id("MasterPass_wallet_frame")));
            driver.FindElement(By.Id("email")).SendKeys("john.berry@accesshq.com");
            driver.FindElement(By.Id("password")).SendKeys("Password1");
            driver.FindElement(By.Id("signInButton")).Click();
            System.Threading.Thread.Sleep(10000);
            //wait.Until(driver => !driver.FindElement(By.Id("MasterPass_animation_overlay")).Displayed);
            driver.FindElement(By.CssSelector("button.button.command")).Click();
        }

        public void CompleteVisaCheckoutPopUpPayment()
        {
            driver.SwitchTo().Frame(driver.FindElement(By.Id("VMECheckoutIframe")));
            driver.FindElement(By.CssSelector("a > span")).Click();
            IWebElement username = driver.FindElement(By.Id("userName"));
            username.SendKeys("john_berry@vrl.com.au");
            IWebElement password = driver.FindElement(By.Id("password"));
            //password.Click();
            password.SendKeys("Village02");
            IWebElement signIn = driver.FindElement(By.ClassName("viewButton-button"));
            signIn.Click();
            IWebElement payButton = driver.FindElement(By.Name("Pay"));
            payButton.Click();
        }
        //Navigate directly to login page and log in
        public void LogInOnDesktop()
        {
            // Navigate to Log In page successfully
            driver.Navigate().GoToUrl("https://awscdn.scuat.villagecinemas.com.au/village-movie-club/login?ReturnUrl=%2f");
            driver.FindElement(By.Id("txtEmailAddress")).SendKeys("johnberry55@gmail.com");
            driver.FindElement(By.Id("txtPassword")).SendKeys("Password1");
            driver.FindElement(By.Id("btnSubmit")).Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(driver => driver.FindElement(By.Id("LoginTexthWidget")).Displayed);
            driver.FindElement(By.Id("LoginTexthWidget")).Click();
        }

        public void NavigateToMyVMC()
        {
            new Actions(driver).MoveToElement(VillageMovieClubLink);
            VillageMovieClubLink.Click();
        }

        //Global Search box on desktop landing page
        public void Search(string textToType)
        {
            SearchBox.Clear();
            SearchBox.SendKeys(textToType);
            new Actions(driver).SendKeys(Keys.Enter).Perform();
        }

        //My VMC Page
        public void ClickEditYourProfile()
        {
            new Actions(driver).MoveToElement(EditYourProfileLink);
            EditYourProfileLink.Click();
        }

        //Edit Profile Page
        public void EnterYourDetailsFirstName(string textToType)
        {
            new Actions(driver).MoveToElement(YourDetailsFirstNameTextBox);
            YourDetailsFirstNameTextBox.Clear();
            YourDetailsFirstNameTextBox.SendKeys(textToType);
        }

        //Edit Profile Page
        public void EnterYourDetailsCurrentPassword(string textToType)
        {
            new Actions(driver).MoveToElement(YourDetailsCurrentPasswordTextBox);
            YourDetailsCurrentPasswordTextBox.Clear();
            YourDetailsCurrentPasswordTextBox.SendKeys(textToType);
        }

        //Edit Profile Page
        public void ClickEditYourProfilePrivacyPolicyCheckbox()
        {
            new Actions(driver).MoveToElement(YourDetailsPrivacyPolicyCheckbox);
            YourDetailsPrivacyPolicyCheckbox.Click();
        }

        //Edit Profile Page
        public void ClickEditYourProfileTermsAndConditionsCheckbox()
        {
            new Actions(driver).MoveToElement(YourDetailsTermsAndConditionsCheckbox);
            YourDetailsTermsAndConditionsCheckbox.Click();
        }

        //Edit Profile Page
        public void ClickEditYourProfileSaveButton()
        {
            new Actions(driver).MoveToElement(EditYourProfileSaveButton);
            EditYourProfileSaveButton.Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(driver => !driver.FindElement(By.Id("ucLoader_imgLoading")).Displayed);
        }

        public void MakePayPalPayment()
        {
            driver.FindElement(By.XPath("//div[@id='pnlPaymentOptions']/ul/li[2]")).Click();
            System.Threading.Thread.Sleep(8000);
            driver.FindElement(By.Id("content_0_chkTerms")).Click();
            System.Threading.Thread.Sleep(8000);
            driver.FindElement(By.Id("btnPayPalCheckout")).Click();
            string oldWindow = driver.WindowHandles[0];
            string newWindow = driver.WindowHandles[1];
            System.Threading.Thread.Sleep(12000);
            driver.Manage().Window.Maximize();
            //The PayPal screen is present at this point  
            driver.SwitchTo().Window(newWindow);
            System.Threading.Thread.Sleep(20000);
            driver.SwitchTo().Frame("injectedUl");
            driver.FindElement(By.Id("email")).Click();
            driver.FindElement(By.XPath("//*[@id='email']")).SendKeys("testing1@profero.com");
            driver.FindElement(By.Id("password")).Click();
            driver.FindElement(By.XPath("//*[@id='password']")).SendKeys("Password1");
            driver.FindElement(By.Id("btnLogin")).Click();         
            System.Threading.Thread.Sleep(14000);
            driver.FindElement(By.XPath("//*[@id='closeCart']")).Click();
            driver.FindElement(By.XPath("//*[@id='confirmButtonTop']")).Click();
            System.Threading.Thread.Sleep(12000);
            driver.SwitchTo().Window(oldWindow);
        }

        public void MakeGiftCardPayment()
        {
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.ArrowDown).Perform();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.ArrowDown).Perform();
            driver.FindElement(By.Id("btnShowAddGiftCard")).Click();
            driver.FindElement(By.Id("txtQuickGiftCardNumber")).SendKeys("9036007706232620");
            driver.FindElement(By.Id("txtQuickGiftCardPin")).SendKeys("1234");
            driver.FindElement(By.Id("btnAddGiftCard")).Click();
            System.Threading.Thread.Sleep(10000);
            driver.FindElement(By.Id("txtEmail")).SendKeys("Fred@gmail.com");
            driver.FindElement(By.Id("txtPhoneNo")).SendKeys("0411111111");
            driver.FindElement(By.Id("content_0_txtYourName")).SendKeys("Fred Flinstone");
            driver.FindElement(By.Id("content_0_chkTerms")).Click();
            driver.FindElement(By.Id("btnBuyTicket")).Click();
        }

        public void ClickVrewardsHeaderLink()
        {
            new Actions(driver).MoveToElement(VrewardsLink);
            VrewardsLink.Click();
        }


    }
}