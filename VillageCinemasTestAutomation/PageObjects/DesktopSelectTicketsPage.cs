﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace VillageCinemasTestAutomation.PageObjects
{
    public class DesktopSelectTicketsPage
    {
        private readonly IWebDriver driver;
        public DesktopSelectTicketsPage(IWebDriver browser)

        {
            this.driver = browser;
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.Id, Using = "ddlQuantity")]
        public IWebElement TicketQty { get; set; }

        [FindsBy(How = How.Id, Using = "btnId")]
        public IWebElement MakePaymentButton { get; set; }

        public void SelectTicketQty()
        {
            TicketQty.Click();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.ArrowDown).Perform();
            new Actions(driver).SendKeys(OpenQA.Selenium.Keys.Return).Perform();
        }

        public void ClickMakePaymentButton()
        {
            new Actions(driver).MoveToElement(MakePaymentButton);
            driver.FindElement(By.Id("btnId")).Click();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(driver => !driver.FindElement(By.Id("ucLoader_imgLoading")).Displayed);
        }
    }
}