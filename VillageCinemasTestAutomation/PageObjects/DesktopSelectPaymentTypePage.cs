﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace VillageCinemasTestAutomation.PageObjects
{
    public class DesktopSelectPaymentTypePage
    {
        private readonly IWebDriver driver;
        public DesktopSelectPaymentTypePage(IWebDriver browser)

        {
            this.driver = browser;
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.Id, Using = "btnBuyTicket")]
        public IWebElement PaymentBuyTicketsButton { get; set; }

        [FindsBy(How = How.Id, Using = "txtEmail")]
        public IWebElement YourEmailAddressTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "txtPhoneNo")]
        public IWebElement YourMobilePhoneNumberTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "content_0_ctl07_txtName")]
        public IWebElement NameOnCardTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "content_0_ctl07_txtCCNumber")]
        public IWebElement CardNumberTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "content_0_ctl07_txtCVV")]
        public IWebElement CVVTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "content_0_ctl07_txtMonth")]
        public IWebElement CardExpiryMonthTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "content_0_ctl07_txtYear")]
        public IWebElement CardExpiryYearTextBox { get; set; }

        [FindsBy(How = How.Id, Using = "content_0_chkTerms")]
        public IWebElement TermsAndConditionsCheckBox { get; set; }


        public void EnterYourEmailAddress(string textToType)
        {
            YourEmailAddressTextBox.Clear();
            YourEmailAddressTextBox.SendKeys(textToType);
        }

        public void EnterYourMobilePhoneNumber(string textToType)
        {
            YourMobilePhoneNumberTextBox.Clear();
            YourMobilePhoneNumberTextBox.SendKeys(textToType);
        }

        public void EnterNameOnCard(string textToType)
        {
            NameOnCardTextBox.Clear();
            NameOnCardTextBox.SendKeys(textToType);
        }

        public void EnterCardNumber(string textToType)
        {
            CardNumberTextBox.Clear();
            CardNumberTextBox.SendKeys(textToType);
        }

        public void EnterCVVNumber(string textToType)
        {
            CVVTextBox.Clear();
            CVVTextBox.SendKeys(textToType);
        }

        public void EnterCardExpiryMonth(string textToType)
        {
            CardExpiryMonthTextBox.Clear();
            CardExpiryMonthTextBox.SendKeys(textToType);
        }

        public void EnterCardExpiryYear(string textToType)
        {
            CardExpiryYearTextBox.Clear();
            CardExpiryYearTextBox.SendKeys(textToType);
        }

        public void ClickTermsAndConditionsCheckBox()
        {
            TermsAndConditionsCheckBox.Click();
        }

        public void ClickPaymentBuyTicketsButton()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));          
            new Actions(driver).MoveToElement(PaymentBuyTicketsButton);
            PaymentBuyTicketsButton.Click();
            wait.Until(driver => !driver.FindElement(By.Id("ucLoader_imgLoading")).Displayed);
        }
    }
}