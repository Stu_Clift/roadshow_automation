﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class BookingHistory
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void VerifyBookingHistoryRemoved()
        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            villageCinemasDesktopObjects.LogInOnDesktop();
            Driver.FindElement(By.Id("LoginTexthWidget")).Click();
            Driver.FindElement(By.XPath("//a[@title='My VMC']"));       
        }
    }
}