﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class GoldClassContent
    { 
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }
        [TestMethod]
        public void GoldClassContentPresent()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://awscdn.scuat.villagecinemas.com.au/gold-class");
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            driver.Manage().Window.Maximize();
            authenticationObject.HandleZscaler();         
            Assert.AreEqual("Gold Class", driver.FindElement(By.XPath("//*[@id='page']/div[3]/h1/span")).Text);
            driver.Dispose();
        }
    }
}