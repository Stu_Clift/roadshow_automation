﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class DeskTopLogIn
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void DesktopLoginSuccessfully()
        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopLoginPage desktopLoginPage = new DesktopLoginPage(Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasDesktopObjects.ClickLoginLink();
            System.Threading.Thread.Sleep(5000);
            desktopLoginPage.EnterLogInEmail("testauto@email.com");
            desktopLoginPage.EnterLogInPassword("Password1");
            desktopLoginPage.ClickLogInButton();
            System.Threading.Thread.Sleep(5000);
            villageCinemasDesktopObjects.ClickVrewardsHeaderLink();
            System.Threading.Thread.Sleep(5000);
            IWebElement body = Driver.FindElement(By.TagName("body"));
            System.Threading.Thread.Sleep(5000);
            Assert.IsTrue(body.Text.Contains("Hi, Fred!"));
            Driver.Dispose();
        }
    }
}