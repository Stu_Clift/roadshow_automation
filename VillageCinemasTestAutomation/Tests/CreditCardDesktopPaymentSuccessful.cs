﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PlayGround;
using OpenQA.Selenium.Chrome;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation
{

    [TestClass]
    public class CreditCardDesktopPayment

    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void CreditCardDesktopPaymentSuccessful()

        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopSelectTicketsPage desktopSelectTicketsPage = new DesktopSelectTicketsPage(Driver);
            DesktopSelectPaymentTypePage desktopSelectPaymentTypePage = new DesktopSelectPaymentTypePage(Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasDesktopObjects.ClickQuickTicketsHeader();
            villageCinemasDesktopObjects.ClickQuickTicketsCinema();
            villageCinemasDesktopObjects.ClickQuickTicketsMovie();
            villageCinemasDesktopObjects.ClickQuickTicketsSession();
            villageCinemasDesktopObjects.ClickQuickTicketsBuyTicketsButton();
            desktopSelectTicketsPage.SelectTicketQty();
            desktopSelectTicketsPage.ClickMakePaymentButton();
            desktopSelectPaymentTypePage.EnterYourEmailAddress("Fred@gmail.com");
            desktopSelectPaymentTypePage.EnterYourMobilePhoneNumber("0411111111");
            desktopSelectPaymentTypePage.EnterNameOnCard("Fred Flinstone");
            desktopSelectPaymentTypePage.EnterCardNumber("4111111111111111");
            desktopSelectPaymentTypePage.EnterCVVNumber("1234");
            desktopSelectPaymentTypePage.EnterCardExpiryMonth("05");
            desktopSelectPaymentTypePage.EnterCardExpiryYear("2017");
            desktopSelectPaymentTypePage.ClickTermsAndConditionsCheckBox();
            desktopSelectPaymentTypePage.ClickPaymentBuyTicketsButton();
            IWebElement body = Driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains("THIS IS YOUR TICKET - ENJOY YOUR MOVIE!"));
            Driver.Dispose();
        }
    }
}