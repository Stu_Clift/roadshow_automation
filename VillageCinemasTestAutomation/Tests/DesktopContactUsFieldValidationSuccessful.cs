﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class DesktopContactUs
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }
        [TestMethod]
        public void DesktopContactUsFieldValidationSuccessful()
        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopContactUsPage desktopContactUsPage = new DesktopContactUsPage(Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            //villageCinemasDesktopObjects.ClickQuickTicketsHeader();
            //villageCinemasDesktopObjects.ClickQuickTicketsCinema();
            //villageCinemasDesktopObjects.ClickQuickTicketsMovie();
            //villageCinemasDesktopObjects.ClickQuickTicketsSession();
            //villageCinemasDesktopObjects.ClickQuickTicketsBuyTicketsButton();
            //desktopSelectTicketsPage.SelectTicketQty();
            //desktopSelectTicketsPage.ClickMakePaymentButton();
            //desktopSelectPaymentTypePage.ClickPaymentBuyTicketsButton();
            //Assert.AreEqual("-Please enter an email address.", Driver.FindElement(By.CssSelector("p")).Text);
            //Assert.AreEqual("-Please enter your contact number", Driver.FindElement(By.XPath("//div[@id='paymentErrorText']/p[2]")).Text);
            //Assert.AreEqual("-Please enter your name.", Driver.FindElement(By.XPath("//div[@id='paymentErrorText']/p[4]")).Text);
            //Assert.AreEqual("-Please enter a card number.", Driver.FindElement(By.XPath("//div[@id='paymentErrorText']/p[5]")).Text);
            //Assert.AreEqual("-Please enter a CVC number.", Driver.FindElement(By.XPath("//div[@id='paymentErrorText']/p[6]")).Text);
            //Assert.AreEqual("-Please enter an expiry month.", Driver.FindElement(By.XPath("//div[@id='paymentErrorText']/p[7]")).Text);
            //Assert.AreEqual("-Please enter an expiry year.", Driver.FindElement(By.XPath("//div[@id='paymentErrorText']/p[8]")).Text);
            //Driver.Dispose();
            //tes
        }
    }
}