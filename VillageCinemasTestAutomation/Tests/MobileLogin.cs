﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class MobileLogin
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void MobileLoginTest()
        {
            VillageCinemasMobileObjects villageCinemasMobileObjects = new VillageCinemasMobileObjects(this.Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasMobileObjects.NavigateToMobileHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasMobileObjects.SelectMobileLoginButton();
            villageCinemasMobileObjects.SetEmail("johnberry55@gmail.com");
            villageCinemasMobileObjects.SetPassword("Password1");
            villageCinemasMobileObjects.ClickMobileLoginButton();
            villageCinemasMobileObjects.SelectMobileMyVmcButton();
            IWebElement body = Driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains("Hi, John!"));
            //Driver.FindElement(By.XPath("//h3[contains(.,'Welcome back John')]"));
            Driver.Dispose();
        }
    }
}