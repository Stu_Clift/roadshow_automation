﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class VisaCheckoutDesktopPayment
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void VisaCheckoutDesktopPaymentSuccessful()
        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopSelectTicketsPage desktopSelectTicketsPage = new DesktopSelectTicketsPage(Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            string oldWindow = Driver.WindowHandles[0];
            villageCinemasDesktopObjects.ClickQuickTicketsHeader();
            villageCinemasDesktopObjects.ClickQuickTicketsCinema();
            villageCinemasDesktopObjects.ClickQuickTicketsMovie();
            villageCinemasDesktopObjects.ClickQuickTicketsSession();
            villageCinemasDesktopObjects.ClickQuickTicketsBuyTicketsButton();
            desktopSelectTicketsPage.SelectTicketQty();
            desktopSelectTicketsPage.ClickMakePaymentButton();
            villageCinemasDesktopObjects.SelectVisaCardPayment();
            villageCinemasDesktopObjects.CompleteVisaCheckoutPopUpPayment();
            Driver.SwitchTo().Window(oldWindow);
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(driver => !driver.FindElement(By.Id("ucLoader_imgLoading")).Displayed);
            IWebElement pageBody = Driver.FindElement(By.Id("pageBody"));
            System.Threading.Thread.Sleep(25000);
            Assert.IsTrue(pageBody.Text.Contains("Booking Confirmation"));
            //Assert.AreEqual("Thanks for booking tickets. This page has been emailed to john_berry@vrl.com.au. Show this code on your mobile or print it out to enter the cinema.", Driver.FindElement(By.Id("content_0_divHeaderContent")).Text);
            System.Threading.Thread.Sleep(20000);
            Driver.Dispose();
        }
    }
}