﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class DesktopUpdateDetails
    {
        
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void DesktopUpdateDetailsSuccessful()
        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopLoginPage desktopLoginPage = new DesktopLoginPage(Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasDesktopObjects.ClickLoginLink();
            desktopLoginPage.EnterLogInEmail("testauto@email.com");
            desktopLoginPage.EnterLogInPassword("Password1");
            desktopLoginPage.ClickLogInButton();
            villageCinemasDesktopObjects.ClickVrewardsHeaderLink();
            //villageCinemasDesktopObjects.NavigateToMyVMC();
            villageCinemasDesktopObjects.ClickEditYourProfile();
            villageCinemasDesktopObjects.EnterYourDetailsFirstName("Fred");
            villageCinemasDesktopObjects.EnterYourDetailsCurrentPassword("Password1");
            villageCinemasDesktopObjects.ClickEditYourProfilePrivacyPolicyCheckbox();
            villageCinemasDesktopObjects.ClickEditYourProfileTermsAndConditionsCheckbox();
            villageCinemasDesktopObjects.ClickEditYourProfileSaveButton();
            Assert.AreEqual("Your profile was updated successfully.", Driver.FindElement(By.CssSelector("h4.no-margin")).Text);
        }
    }
}
