﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Linq;
using System.Drawing;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class PayPalMobilePayment
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void PayPalMobilePaymentSuccessful()
        {
            VillageCinemasMobileObjects villageCinemasMobileObjects = new VillageCinemasMobileObjects(this.Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasMobileObjects.NavigateToMobileHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasMobileObjects.SelectMobileTicket();
            villageCinemasMobileObjects.SelectQtyMobileTickets();
            villageCinemasMobileObjects.MakePayPalMobilePayment();
            Assert.AreEqual("Show this code on your phone or print it out to access the cinema.  Booking details have also been emailed to you.", Driver.FindElement(By.CssSelector("li.row > div.container > p")).Text);
            Driver.Dispose();
        }
    }
}