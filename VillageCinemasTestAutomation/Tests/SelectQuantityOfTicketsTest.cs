﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PlayGround;
using OpenQA.Selenium.Chrome;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation
{

    [TestClass]
    public class SelectQuantityOfTickets

    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }
        [TestMethod]
        public void SelectQuantityOfTicketsTest()
        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopSelectTicketsPage desktopSelectTicketsPage = new DesktopSelectTicketsPage(this.Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasDesktopObjects.ClickQuickTicketsHeader();
            villageCinemasDesktopObjects.ClickQuickTicketsCinema();
            villageCinemasDesktopObjects.ClickQuickTicketsMovie();
            villageCinemasDesktopObjects.ClickQuickTicketsSession();
            villageCinemasDesktopObjects.ClickQuickTicketsBuyTicketsButton();          
            desktopSelectTicketsPage.SelectTicketQty();
            Driver.Dispose();
        }
    }
}