﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{
    [TestClass]
    public class VmaxPageContent
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }
        [TestMethod]
        public void VmaxPageContentPresent()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://awscdn.scuat.villagecinemas.com.au/vmax");
            driver.Manage().Window.Maximize();
            Assert.AreEqual("Vmax is the ultimate way to experience blockbuster movies. With supersized screens, Dolby Digital sound, first class stadium seating, extra leg room and wide armrests; immerse yourself in the movie with Vmax!", driver.FindElement(By.CssSelector("p")).Text);
            Assert.AreEqual("Vmax cinema screens are located at Village Cinemas Crown, Doncaster, Fountain Gate, Jam Factory, Knox, Southland, Sunshine, Century City, and Karingal. Our biggest Vmax screen at Knox is 28 metres wide and seats over 740 movie-goers.", driver.FindElement(By.XPath("//div[@id='ContentLeftSide']/div/div/p[2]")).Text);
            Assert.AreEqual("Now showing at Vmax", driver.FindElement(By.CssSelector("h1.center-align.vMargin10")).Text);
            driver.Dispose();
        }
    }
}