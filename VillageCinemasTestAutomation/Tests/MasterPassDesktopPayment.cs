﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;


namespace VillageCinemasTestAutomation.Tests
{

    [TestClass]
    public class MasterPassDesktopPayment

    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void MasterPassDesktopPaymentSuccessful()

        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopSelectTicketsPage desktopSelectTicketsPage = new DesktopSelectTicketsPage(Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasDesktopObjects.ClickQuickTicketsHeader();
            villageCinemasDesktopObjects.ClickQuickTicketsCinema();
            villageCinemasDesktopObjects.ClickQuickTicketsMovie();
            villageCinemasDesktopObjects.ClickQuickTicketsSession();
            villageCinemasDesktopObjects.ClickQuickTicketsBuyTicketsButton();
            desktopSelectTicketsPage.SelectTicketQty();
            desktopSelectTicketsPage.ClickMakePaymentButton();          
            string oldWindow = Driver.WindowHandles[0];
            villageCinemasDesktopObjects.SelectMasterCardPayment();
            Driver.SwitchTo().Frame(Driver.FindElement(By.Id("MasterPass_frame")));
            villageCinemasDesktopObjects.CompleteMasterCardPopUpPayment();
            Driver.SwitchTo().Window(oldWindow);
            Assert.AreEqual("Thanks for booking tickets. This page has been emailed to john.berry@accesshq.com. Show this code on your mobile or print it out to enter the cinema.", Driver.FindElement(By.Id("content_0_divHeaderContent")).Text);
            Driver.Dispose();
        }
    }
}