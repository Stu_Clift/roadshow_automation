﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using VillageCinemasTestAutomation.TestDataAccess;
using OpenQA.Selenium.Support.UI;
using VillageCinemasTestAutomation.PageObjects;

namespace VillageCinemasTestAutomation.Tests
{

    [TestClass]
    public class GiftCardDesktopPayment

    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()

        {
            this.Driver = new ChromeDriver();
            this.Wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(40));
            this.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(40));
            Driver.Manage().Window.Maximize();
        }

        [TestCleanup]
        public void TeardownTest()

        {
            this.Driver.Quit();
        }

        [TestMethod]
        public void GiftCardDesktopPaymentSuccessful()

        {
            VillageCinemasDesktopObjects villageCinemasDesktopObjects = new VillageCinemasDesktopObjects(this.Driver);
            DesktopSelectTicketsPage desktopSelectTicketsPage = new DesktopSelectTicketsPage(Driver);
            AuthenticationObject authenticationObject = new AuthenticationObject(Driver);
            villageCinemasDesktopObjects.NavigateToDesktopHomePage();
            authenticationObject.HandleZscaler();
            villageCinemasDesktopObjects.ClickQuickTicketsHeader();
            villageCinemasDesktopObjects.ClickQuickTicketsCinema();
            villageCinemasDesktopObjects.ClickQuickTicketsMovie();
            villageCinemasDesktopObjects.ClickQuickTicketsSession();
            villageCinemasDesktopObjects.ClickQuickTicketsBuyTicketsButton();
            desktopSelectTicketsPage.SelectTicketQty();
            desktopSelectTicketsPage.ClickMakePaymentButton();
            villageCinemasDesktopObjects.MakeGiftCardPayment();
            IWebElement body = Driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains("THIS IS YOUR TICKET - ENJOY YOUR MOVIE!"));
            Driver.Dispose();
        }
    }
}