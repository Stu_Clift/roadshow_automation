﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VillageCinemasTestAutomation.TestDataAccess
{
  public class GiftCardData
    {
        public string Key { get; set; }
        public string GiftCardNumber { get; set; }
        public string Expiry { get; set; }
        public string Pin { get; set; }
        public string Amount { get; set; }
    }
}