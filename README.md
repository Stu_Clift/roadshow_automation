# README #

This repository stores Automated Test Scripts for testing the following websites:

http://villagecinemas.com.au/
http://villagecinemas.com.au/?lg=Mobile

The scripts are currently configured to point to the UAT website for regression testing purposes.

http://awscdn.scuat.villagecinemas.com.au/


### What is this repository for? ###

* Test the functionality of Village Cinemas website on the desktop and mobile version.
* Version 1

### How do I get set up? ###

* Install Visual Studio IDE or equivalent for running or creating tests. 
* Install Git for Windows.
* Install Selenium C# drivers.
* Initialize a Git repository on your local machine and clone the project.
* The test cases themselves can be run within Visual Studio by right clicking on a test and selecting "Run Selected Tests". In the future these scripts will be scheduled from a build tool and configured so that anyone can click a button to run the regression tests.

### Contribution guidelines ###

* Writing tests - Test Analysts/Developers working for Village Cinemas.
* Code review - Create Pull Requests for every fix/push to the repository.

### Who do I talk to? ###

* Melina Hayashi/John Berry